﻿using System;

namespace Lesson_shpak_28._09._2021
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();
            Pistol pistol = new Pistol();
            MachineGun machineGun = new MachineGun();
            
            


            while (true)
            {
                ConsoleKeyInfo consoleKeyInfo = Console.ReadKey(true);
                Console.Clear();

                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Spacebar:

                        Console.WriteLine("Key \"spacebar\" - Player shoot");

                        player.UseWeapon();

                        break;

                    case ConsoleKey.D0:
                        player.SetWeapon(null);
                        Console.WriteLine("Key \"0\" - Player without weapon");
                        break;


                    case ConsoleKey.D1:
                        player.SetWeapon(pistol);
                        Console.WriteLine("Key \"1\" - Choose Pistol");
                        break;

                    case ConsoleKey.D2:
                        player.SetWeapon(machineGun);
                        Console.WriteLine("Key \"2\" - Choose MachuneGun");
                        break;

                    case ConsoleKey.Escape:
                        Console.WriteLine("Key\"Escape\" - Close program");
                        break;

                }


            }



        }
    }
}
