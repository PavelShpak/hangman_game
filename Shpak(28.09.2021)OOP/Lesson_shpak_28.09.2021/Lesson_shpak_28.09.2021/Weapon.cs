﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_shpak_28._09._2021
{
    public abstract class Weapon : IDisplayable
    {
        protected string name = null;
        public static int count = 0;

        public abstract void Display();
        public abstract void Shoot();

        public static void GetName()
        {

        }
    }

    public interface IDisplayable
    {
       void Display();


    }
}
