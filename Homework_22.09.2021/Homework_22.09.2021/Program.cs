﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Homework_22._09._2021
{
    class Person
    {
       
        public string name;
        public string family;
        public int age;
        public string gender;
       
       
        public void GetInfo()
        {
            Console.WriteLine($"Имя: {name} Фамилия: {family}  Возраст: {age} Пол: {gender} ");
        }


    }
    


    class Program
    {
        static void Main(string[] args)
        {

            Person tom = new Person { name = "Tom", family = "Hanks", age = 34, gender = "Male" }; //передача значений при помощи инициализаторов
           // tom.GetInfo();
          //  Console.WriteLine();

            Person ann = new Person { name = "Ann", family = "Springsteen", age = 38, gender = "Female" };
          //  ann.GetInfo();
          //  Console.WriteLine();


            Person[] persons = { tom, ann };

            for(int i = 0; i < persons.Length; i++)
            {
                persons[i].GetInfo();
            }



        }
    }
}

