﻿using System;
using System.IO;
namespace tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\User\source\repos\Hangman/word_rus.txt";
            string secretword = GetWord(path);

            char[] charWord = new char[secretword.Length];
            int error_count = 10;
            int closedLetters = secretword.Length;


            for (int i = 0; i < charWord.Length; i++)
            {
                charWord[i] = '*';
            }

            while (error_count > 0 && new string(charWord) != secretword)
            {
                Console.WriteLine(charWord);
                Console.WriteLine("Введите букву");
                char letter = Console.ReadLine()[0];
                int count = 0;

                for (int i = 0; i < secretword.Length; i++)
                {
                    if (letter == secretword[i])
                    {
                        closedLetters--;
                        charWord[i] = letter;
                        count++;
                    }
                }
                Console.Clear();
                if (count == 0)
                {

                    error_count--;
                    Console.WriteLine($"Нет такой буквы. У вас осталось {error_count} попыток ");
                }
                else
                {
                    Console.WriteLine("Молодец! Есть такая буква");
                }



            }

            Console.Clear();
            if (error_count == 0)
            {
                Console.WriteLine("Вы проиграли");
            }
            else
            {
                Console.WriteLine("Вы выиграли");
            }
            Console.WriteLine($"Слово - {secretword}");

        }
        static string GetWord(string path)
        {
            string[] secretword = File.ReadAllLines(path);

            string z = secretword[new Random().Next(0, secretword.Length)];

            return z;



        }

    }
}
