﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class Pencil : Uneatable
    {
        public Pencil(string name, float cost)
        {
            this.Name = name;
            this.Cost = cost;
        }
        public void Draw()
        {
            Console.WriteLine("I can Draw!");
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}");
        }
    }
}
