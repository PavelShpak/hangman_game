﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public enum IceCreamType
    {
        Strawberry,
        Peach
    }
    public class IceCream : Eatable
    {
        private IceCreamType Type;
        public IceCream(string name, float cost, IceCreamType type, int expiration)
        {
            this.Name = name;
            this.Cost = cost;
            this.Type = type;
            this.Expiration = expiration;
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}, тип мороженного {Type}, срок годности {Expiration}.");
        }
    }
}
