﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class Item
    {
        protected string Name;
        protected float Cost;
        public virtual void Show()
        {

        }
        public float GetCost()
        {
            return Cost;
        }
    }
}
