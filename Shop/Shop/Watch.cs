﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class Watch : Uneatable
    {
        public Watch(string name, float cost)
        {
            this.Name = name;
            this.Cost = cost;
        }
        public void WriteTime()
        {
            Console.WriteLine(DateTime.Now);
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}");
        }
    }
}
