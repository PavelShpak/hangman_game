﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public enum MeatType
    {
        Pork,
        Turkey
    }

    public class Meat : Eatable
    {
        private MeatType Type;
        public Meat(string name, float cost, MeatType type, int expiration)
        {
            this.Name = name;
            this.Cost = cost;
            this.Type = type;
            this.Expiration = expiration;
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}, тип мяса {Type}, срок годности {Expiration} ");
        }
    }
}
