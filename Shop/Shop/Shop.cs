﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    enum ETypeItem
    {
        meat = 1,
        iceCream = 2,
        pencil = 3,
        watch = 4
    }
    public class Shop : IShop
    {
        private List<Item> products = new List<Item>();
        public void AddItem()
        {

        }
        
        public void AddProduct(Item product)
        {
            products.Add(product);
            Console.WriteLine("Product added!");
        }

        public void ShowCheapest()
        {
            Item temp = new Item();
            float tempF = (float)(3.4 * Math.Pow(10, 38));
            foreach (Item item in products)
            {
                if (item.GetCost() < tempF)
                {
                    tempF = item.GetCost();
                    temp = item;
                }
            }
            temp.Show();
        }

        public void ShowMostExpensiveItem()
        {
            Item temp = new Item();
            float tempF = 0f;
            foreach (Item item in products)
            {
                if (item.GetCost() > tempF)
                {
                    tempF = item.GetCost();
                    temp = item;
                }
            }
            temp.Show();
        }

        public void ShowEatable()
        {
            foreach (Item item in products)
            {
                if (item is Eatable)
                {
                    item.Show();
                }
            }
        }

        public void ShowUneatable()
        {
            foreach (Item item in products)
            {
                if (item is Uneatable)
                {
                    item.Show();
                }
            }
        }
        public void Execute()
        {
            foreach (Item item in products)
            {
                if (item is Pencil)
                {
                    Pencil pencil = item as Pencil;
                    pencil.Draw();
                }
                else if (item is Watch)
                {
                    Watch watch = item as Watch;
                    watch.WriteTime();
                }
            }
        }
    }
}
