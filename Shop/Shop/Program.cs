﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop();
            shop.AddProduct(new Watch("AppleWatch", 10.20f));
            shop.AddProduct(new Meat("Fresh_Meat", 1.75f, MeatType.Pork, 7));
            shop.AddProduct(new IceCream("FruitIce", 0.33f, IceCreamType.Strawberry, 10));
            shop.AddProduct(new Pencil("Parker", 9.45f));
            shop.ShowEatable();
            shop.ShowUneatable();
            Console.WriteLine("Самый дорогой товар:");
            shop.ShowMostExpensiveItem();
            Console.WriteLine("Самый дешевый товар:");
            shop.ShowCheapest();
            shop.Execute();
        }
    }
}
